<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $author
 * @property int $category
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['author', 'category', 'status' ], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function behaviors() {
        return [
            BlameableBehavior::className(),
        ];
    } 
    
    
    public function beforeSave($insert)
    {
        //preventing from users without editor or author permission
        //change from draft to publish
        if (parent::beforeSave($insert)) {
            if (\Yii::$app->user->can('editor')) {
                return true;            
            } else {
                if($this->status == 1){
                    return true;  
                } else {
                    return false;     
                }
            }
        }
        return false;
    }        
        
    
    public function getCategory1()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    } 
    
    public function getStatus1()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }    
    
    public function getAuthor1()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }     

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'author' => 'Author',
            'category' => 'Category',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
